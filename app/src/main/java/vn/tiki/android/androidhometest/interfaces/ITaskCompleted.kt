package vn.tiki.android.androidhometest.interfaces

import vn.tiki.android.androidhometest.data.api.response.Deal

interface ITaskCompleted {
    fun onLoadDataCompleted(result: List<Deal>?)
}
