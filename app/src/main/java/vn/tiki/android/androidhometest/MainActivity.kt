package vn.tiki.android.androidhometest

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import vn.tiki.android.androidhometest.adapter.DealAdapter
import vn.tiki.android.androidhometest.data.api.ApiServices
import vn.tiki.android.androidhometest.data.api.response.Deal
import vn.tiki.android.androidhometest.di.initDependencies
import vn.tiki.android.androidhometest.di.inject
import vn.tiki.android.androidhometest.di.releaseDependencies
import vn.tiki.android.androidhometest.interfaces.ITaskCompleted
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity(), ITaskCompleted {
    override fun onLoadDataCompleted(result: List<Deal>?) {
        loadingView.visibility = GONE
        if (result != null) {
            mDeals.clear()
            mDeals.addAll(result.toMutableList())
            adapter.notifyDataSetChanged()
        }
    }

    private var mDeals: MutableList<Deal> = mutableListOf()
    private lateinit var adapter: DealAdapter
    private lateinit var loadingView: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDependencies(this)

        initView()
        getDataFromServer()
    }

    private fun initView() {
        setContentView(R.layout.activity_main)
        loadingView = findViewById(R.id.loading_view)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        adapter = DealAdapter(mDeals)
        val gridLayoutManager = GridLayoutManager(this, 2)
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                gridLayoutManager.orientation)
        recyclerView.addItemDecoration(dividerItemDecoration)
    }

    private fun getDataFromServer() {
        loadingView.visibility = VISIBLE
        val callbackRef = WeakReference<MainActivity>(this)
        LoadDataAsyncTask(callbackRef).execute()
    }

    class LoadDataAsyncTask(private val callbackRef: WeakReference<MainActivity>) : AsyncTask<Unit, Unit, List<Deal>>() {
        private val apiServices by inject<ApiServices>()

        override fun doInBackground(vararg params: Unit?): List<Deal> {
            return apiServices.getDeals()
        }

        override fun onPostExecute(result: List<Deal>?) {
            super.onPostExecute(result)
            print(result)
            callbackRef.get()?.onLoadDataCompleted(result)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseDependencies()
    }
}
